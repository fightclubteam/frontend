// http://eslint.org/docs/user-guide/configuring

module.exports = {
    root: true,
    parser: 'babel-eslint',
    parserOptions: {
        sourceType: 'module'
    },
    env: {
        browser: true,
    },
    extends: 'airbnb-base',
    // required to lint *.vue files
    plugins: [
        'html',
        'flowtype-errors'
    ],
    // check if imports actually resolve
    'settings': {
        'import/resolver': {
            'webpack': {
                'config': 'build/webpack.base.conf.js'
            }
        }
    },
    // add your custom rules here
    'rules': {
        // don't require .vue extension when importing
        'import/extensions': ['error', 'always', {
            'js': 'never',
            'vue': 'never'
        }],
        // allow optionalDependencies
        'import/no-extraneous-dependencies': ['error', {
            'optionalDependencies': ['test/unit/index.js']
        }],
        // allow debugger during development
        'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
        'no-console': process.env.NODE_ENV === 'production' ? 2 : 0,
        // fixme: fix indent
        // 'indent': [2, 4],
        "indent": 0,
        'flowtype-errors/show-errors': 2,
        "no-underscore-dangle": [2, { "allow": ["_id"] }],
        "no-plusplus": [2, {
            "allowForLoopAfterthoughts": true
        }],
        "max-len": [2, 120, 2, {
            "ignoreComments": true,
            "ignoreUrls": true,
            "ignorePattern": ""
        }],
    }
};

