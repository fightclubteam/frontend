import * as _ from 'lodash';

import { CURRENT_USER } from '../components/Inventory/queries';
import store from '../store';

export default {
    auth: {
        request(req, token) {
            /* eslint no-underscore-dangle: ["error", { "allow": ["_setHeaders"] }] */
            this.options.http._setHeaders.call(this, req, { Authorization: `Bearer ${token}` });
        },
        response({ data: { token } }) {
            if (token) {
                store.commit('SET_TOKEN', token);
            }
            return token;
        },
    },
    http: require('@websanova/vue-auth/drivers/http/vue-resource.1.x.js'), // eslint-disable-line global-require
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'), // eslint-disable-line global-require
    loginData: { url: 'token', fetchUser: true },
    refreshData: { url: 'token', method: 'PUT', enabled: true, interval: 120 },
    fetchData: {
        url: 'q',
        method: 'POST',
        enabled: true,
        body: {
            query: CURRENT_USER,
            variables: {
                withItems: true,
            },
        },
    },
    logoutData: { url: 'token', method: 'DELETE', redirect: { name: 'login' }, makeRequest: true },
    rolesVar: 'role',
    tokenName: 'auth_token',
    authRedirect: { name: 'login' },
    parseUserData(userData) {
        const data = _.get(userData, 'data.currentUser');
        store.commit('SET_USER', data);
        return data;
    },
};
