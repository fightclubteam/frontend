import * as gql from 'graphql-tag';

export const REGISTER_USER = gql`mutation ($signUpUser: UserInput!) {
  registerUser(signUpUser:$signUpUser) {
    id
    email
    login
    name
    sex
  }
}`;

export const FORGOT_PASSWORD = gql`mutation ($email: String!) {
  forgotUserPassword(email:$email) {
    sent_at
    expires_at
  }
}`;
