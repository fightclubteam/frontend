import * as gql from 'graphql-tag';
import { fightFragment, battleFragment, battleLogFragment } from './fragmets';

export const FIGHT_REJECTED = gql`subscription fightRejected($location: String!, $fight_type: FightTypeEnum) {
  fightRejected(location: $location, fight_type: $fight_type) {
      fight_id
      reason_type
  }
}`;

export const FIGHT_DECLINED = gql`subscription fightDeclined($location: String!, $fight_type: FightTypeEnum!) {
  fightDeclined(location: $location, fight_type: $fight_type) {
      fight_id
  }
}`;

export const FIGHT_CREATED = gql`subscription fightCreated($location: String!, $fight_type: FightTypeEnum!) {
  fightCreated (location: $location, fight_type: $fight_type) {
    ${fightFragment}
  }
}`;

export const FIGHT_JOINED = gql`subscription fightJoined($location: String!, $fight_type: FightTypeEnum!) {
  fightJoined (location: $location, fight_type: $fight_type) {
    ${fightFragment}
  }
}`;

export const FIGHT_REFUSED = gql`subscription fightRefused($location: String!, $fight_type: FightTypeEnum!) {
  fightRefused (location: $location, fight_type: $fight_type) {
    ${fightFragment}
  }
}`;

export const BATTLE_STARTED = gql`subscription battleStarted($login: String!) {
  battleStarted(login: $login) {
      fight_id
  }
}`;
export const BATTLE_ENDED = gql`subscription battleEnded($login: String!) {
  battleEnded(login: $login) {
      exp
      inflicted_damage
      taken_damage
      victory_status
  }
}`;

export const BLOWS_CHANGED = gql`subscription blowsChanged($login: String!) {
  blowsChanged(login: $login) {
      ${battleFragment}
  }
}`;

export const BATTLE_LOG_ADDED = gql`subscription battleLogAdded($fight_id: String!) {
  battleLogAdded(fight_id: $fight_id) {
      ${battleLogFragment}
  }
}`;
