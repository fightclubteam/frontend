import * as gql from 'graphql-tag';
import { fightFragment, battleFragment } from './fragmets';

export const CREATE_FIGHT = gql`mutation createFight(
    $timeout: Int!,
    $battle_type: BattleTypeEnum!,
    $fight_type: FightTypeEnum!
    ) {
  createFight(timeout: $timeout, battle_type: $battle_type, fight_type: $fight_type) {
    ${fightFragment}
  }
}`;

export const DECLINE_FIGHT = gql`mutation declineFight($fight_id: String!) {
  declineFight(fight_id: $fight_id) {
    fight_id
  }
}`;

export const FIGHT_RESPOND = gql`mutation respondFight($login: String!, $approve: Boolean! $fight_id: String!) {
  respondFight(login: $login, approve: $approve, fight_id: $fight_id) {
    ${fightFragment}
  }
}`;

export const REFUSE_FIGHT = gql`mutation refuseFight($fight_id: String!) {
  refuseFight(fight_id: $fight_id) {
    ${fightFragment}
  }
}`;

export const JOIN_FIGHT = gql`mutation joinFight($team: FightParticipantTeamEnum!, $fight_id: String!) {
  joinFight(team: $team, fight_id: $fight_id) {
    ${fightFragment}
  }
}`;

export const ATTACK = gql`mutation attack(
        $login: String!,
        $attack: [FightActionBodyPartEnum]!,
        $block: [FightActionBodyPartEnum]!
    ) {
  attack(login: $login, attack: $attack, block: $block) {
    ${battleFragment}
  }
}`;
