import * as _ from 'lodash';
import { FIGHT_REJECTED, FIGHT_CREATED, FIGHT_DECLINED, FIGHT_JOINED, FIGHT_REFUSED } from './subscriptions';

function getFightsTypeListSubscriber(subscription, count) {
    this.$parent.$apollo.queries.fightTypesListCount.subscribeToMore({
        document: subscription,
        variables: {
            location: this.$auth.user().location._id,
            login: this.$auth.user().login,
            fight_type: this.$route.meta.fight_type,
        },
        updateQuery: ({ fightTypesListCount }) => {
            const fightsTypeCount = fightTypesListCount[this.$route.meta.fight_type] + count;
            return {
                fightTypesListCount: {
                    ...fightTypesListCount,
                    [this.$route.meta.fight_type]: fightsTypeCount < 0 ? 0 : fightsTypeCount,
                },
            };
        },
    });
}

export default {
    computed: {
        canCreateFight() {
            const user = this.$auth.user();

            if (user.hp_current < (user.hp_max / 3)) {
                return false;
            }

            const { _id: id, login } = user;
            for (let i = 0; i < this.fights.length; i++) {
                const fight = this.fights[i];
                if (fight.created_by === id ||
                    _.find(fight.team.right, { login }) ||
                    _.find(fight.team.left, { login })
                ) {
                    return false;
                }
            }
            return true;
        },
    },
    mounted() {
        const user = this.$auth.user();
        this.$apollo.queries.fights.subscribeToMore({
            document: FIGHT_REJECTED,
            variables: {
                location: user.location._id,
                fight_type: this.$route.meta.fight_type,
            },
            updateQuery: (previousResult, { subscriptionData }) => ({
                fights: _.reject(
                    previousResult.fights,
                    { _id: subscriptionData.data.fightRejected._id },
                ).concat(subscriptionData.data.fightRejected),
            }),
        });

        this.$apollo.queries.fights.subscribeToMore({
            document: FIGHT_DECLINED,
            variables: {
                location: user.location._id,
                fight_type: this.$route.meta.fight_type,
            },
            updateQuery: (previousResult, { subscriptionData }) => ({
                fights: _.reject(
                    previousResult.fights,
                    { _id: subscriptionData.data.fightDeclined.fight_id },
                ),
            }),
        });
        this.$apollo.queries.fights.subscribeToMore({
            document: FIGHT_JOINED,
            variables: {
                location: user.location._id,
                fight_type: this.$route.meta.fight_type,
            },
            updateQuery: (previousResult, { subscriptionData }) => ({
                fights: _.reject(
                    previousResult.fights,
                    { _id: subscriptionData.data.fightJoined._id },
                ).concat(subscriptionData.data.fightJoined),
            }),
        });

        this.$apollo.queries.fights.subscribeToMore({
            document: FIGHT_REFUSED,
            variables: {
                location: user.location._id,
                fight_type: this.$route.meta.fight_type,
            },
            updateQuery: (previousResult, { subscriptionData }) => ({
                fights: _.reject(
                    previousResult.fights,
                    { _id: subscriptionData.data.fightRefused._id },
                ).concat(subscriptionData.data.fightRefused),
            }),
        });

        this.$apollo.queries.fights.subscribeToMore({
            document: FIGHT_CREATED,
            variables: {
                location: user.location._id,
                fight_type: this.$route.meta.fight_type,
            },
            updateQuery: (previousResult, { subscriptionData }) => {
                if (_.find(previousResult.fights, { _id: subscriptionData.data.fightCreated._id })) {
                    return previousResult;
                }
                return {
                    fights: [
                        ...previousResult.fights,
                        subscriptionData.data.fightCreated,
                    ],
                };
            },
        });

        getFightsTypeListSubscriber.call(this, FIGHT_CREATED, 1);
        getFightsTypeListSubscriber.call(this, FIGHT_REJECTED, -1);
        getFightsTypeListSubscriber.call(this, FIGHT_DECLINED, -1);
    },
};
