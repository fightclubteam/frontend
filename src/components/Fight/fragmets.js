// todo: use real GraphQL fragments
// todo: use GraphQL directives, we should prevent sending teams data for Chaotic fights
export const fightFragment = `
    _id
    created_by {
        login
        level
    }
    fight_type
    battle_type
    timeout
    left_count
    left_type
    right_count
    right_type
    status
    team_winner
    team {
        left {
            login
            level
            status
            created_at
        }
        right {
            login
            level
            status
            created_at
        }
    }
    comment
    begins_in
    finished_at
    created_at
`;

export const battleLogFragment = `
    _id
    action
    attacker
    defender
    login
    inflicted_damage
    created_at
    log
    hp {
        current
        max
    }
    body_part {
        attack
        block
    }
`;

export const battleFragment = `
    inflicted_damage
    taken_damage
    is_dead
    timeout
    attack_count_places
    defend_count_places
    rounds {
        round
        is_left_user_dodge
        is_right_user_dodge
        is_left_user_critical
        is_right_user_critical
        login_right
        login_left
    }
    attack {
        _id
        team {
            left {
                login
                level
                hp_max
                hp_current
            }
            right {
                login
                level
                hp_max
                hp_current
            }
        }
        user_right {
            login
            level
            hp_max
            hp_current
        }
        user_left {
            login
            level
            hp_max
            hp_current
            tactics {
                hitting
                critical
                parry
                block
                inflicted_damage
                spirituality_sincerity
                counterblow
            }
        }
        created_at
    }
`;
