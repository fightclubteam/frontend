import * as gql from 'graphql-tag';
import { fightFragment, battleFragment, battleLogFragment } from './fragmets';

export const FIGHT_TYPE_LIST_COUNT = gql`{
   fightTypesListCount {
       CHAOTIC
       CURRENT
       FINISHED
       GROUP
       PHYSICAL
   }
}`;

export const FIGHTS = gql`query ($fight_type: FightTypeRoomEnum!, $userId: String) {
    fights (fight_type: $fight_type, userId: $userId) {
        ${fightFragment}
    }
}`;

export const BATTLE = gql`query battle($fight_id: String!) {
    battle (fight_id: $fight_id) {
        ${battleFragment}
    }
}`;

export const BATTLE_LOGS = gql`query battleLogs($fight_id: String!) {
    battleLogs (fight_id: $fight_id) {
        ${battleLogFragment}
    }
}`;

export const BATTLE_FULL_LOGS = gql`query battleFullLogs($fight_id: String!) {
    battleFullLogs (fight_id: $fight_id) {
        _id
        team {
            left {
                login
                level
                hp_max
                hp_current
            }
            right {
                login
                level
                hp_max
                hp_current
            }
        }
        rounds {
            ${battleLogFragment}
        }
    }
}`;
