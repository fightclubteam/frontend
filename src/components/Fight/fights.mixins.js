import * as _ from 'lodash';
import { FIGHTS } from './queries';

export default {
    data() {
        return {
            fights: [],
        };
    },
    computed: {
        orderedFights() {
            return _.orderBy(this.fights, ['created_at'], ['desc']);
        },
    },
    apollo: {
        fights: {
            query: FIGHTS,
            fetchPolicy: 'network-only',
            variables() {
                return {
                    fight_type: this.$route.meta.fight_type,
                };
            },
        },
    },
};
