import * as gql from 'graphql-tag';

export const UPDATE_USER_STATS =
    gql`mutation updateUserStats($abilities: UserAbilityInput!, $mastery: UserMasteryInput!) {
  updateUserStats(
    abilities: $abilities,
    mastery: $mastery
  )
}`;

export const TAKE_CLOTHES_OFF = gql`mutation takeClothesOff {
  takeClothesOff {
    id
  }
}`;

export const TAKE_CLOTH_ON = gql`mutation takeClothOn($item_id: String!) {
  takeClothOn(item_id: $item_id) {
    id
  }
}`;
export const TAKE_SET_ON = gql`mutation takeSetOn($set_id: String!) {
  takeSetOn(set_id: $set_id) {
    id
  }
}`;

export const TAKE_CLOTH_OFF = gql`mutation takeClothOff($item_id: String!) {
  takeClothOff(item_id: $item_id) {
    id
  }
}`;

export const CREATE_SET = gql`mutation createSet($name: String!, $items: [String!]!, $set_id: String) {
  createSet(name: $name, items: $items, set_id: $set_id) {
    id
  }
}`;

export const DELETE_SET = gql`mutation deleteSet($set_id: String!) {
  deleteSet(set_id: $set_id) {
    id
  }
}`;
