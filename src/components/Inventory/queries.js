import * as gql from 'graphql-tag';

import * as storeFragments from '../Store/fragments';

export const CURRENT_USER = gql`
    query (
    $itemsPage: Int = 1,
    $itemsLimit: Int = 10,
    $withItems: Boolean = true
    ) {
        currentUser {
            ...UserFragment
        }
    }

    ${storeFragments.user}

`;

export const USERS_LIST = gql`
    query {
        users {
            ...UserFragment
        }
    }
`;
