import * as gql from 'graphql-tag';

import * as fragments from './fragments';

export const STORE_BUY_ITEM = gql`
    mutation ($storeId: String!, $itemId: String!) {
        buyItemFromStore(store_id:$storeId, store_item_id:$itemId) {
            item {
                ...ItemTypeFragment
            }
        }
    }

    ${fragments.item}

`;

export const STORE_SELL_ITEM = gql`
    mutation ($storeId: String!, $itemId: String!) {
        sellItemToStore(store_id:$storeId, user_item_id:$itemId) {
            item {
                ...ItemTypeFragment
            }
        }
    }

    ${fragments.item}

`;

export const STORE_CREATE_ITEM = gql`
    mutation ($item: ItemInputType!) {
        createItem(item:$item) {
            ...ItemTypeFragment
        }
    }

    ${fragments.item}

`;

export const STORE_ADD_ITEM = gql`
    mutation (
    $storeId: String!,
    $itemId: String!,
    $withItems: Boolean = false,
    $withMyItems: Boolean = false,
    $itemsPage: Int = 1,
    $itemsLimit: Int = 10,
    $itemsFilters: ItemFilterType
    ) {
        addItemToStore(store_id:$storeId, item_id:$itemId) {
            ...StoreTypeFragment
        }
    }

    ${fragments.store}

`;
