import * as gql from 'graphql-tag';

import * as fragments from './fragments';

export const CURRENT_USER = gql`
    query (
    $itemsPage: Int = 1,
    $itemsLimit: Int = 10,
    $withItems: Boolean = true
    ) {
        currentUser {
            ...UserFragment
        }
    }

    ${fragments.user}

`;

export const STORE_BY_SLUG = gql`
    query (
    $slug: String!,
    $withItems: Boolean = true,
    $withMyItems: Boolean = true,
    $itemsPage: Int = 1,
    $itemsLimit: Int = 10,
    $itemsFilters: ItemFilterType
    ) {
        store:storeBySlug(slug: $slug) {
            ...StoreTypeFragment
        }
    }

    ${fragments.store}

`;

export const STORES_LIST = gql`
    query (
    $withItems: Boolean = true,
    $withMyItems: Boolean = true,
    $itemsPage: Int = 1,
    $itemsLimit: Int = 10,
    $itemsFilters: ItemFilterType
    ) {
        stores {
            ...StoreTypeFragment
        }
    }

    ${fragments.store}
`;

export const ITEM_TYPES_LIST = gql`{
    itemTypes {
        item_type
        title
    }
}`;

export const USER_RANKS_LIST = gql`{
    userRanks {
        slug
        position
        title
    }
}`;
