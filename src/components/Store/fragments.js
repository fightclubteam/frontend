import * as gql from 'graphql-tag';

export const item = gql`
    fragment ItemTypeFragment on ItemType {
        _id
        item_type
        title
        description
        weight
        durability {
            current
            max
        }
        mastery {
            club
            axe
            knife
            sword
        }
        restrictions {
            min_user_level
            min_user_rank
            min_strength
            min_agility
            min_intuition
            min_endurance
        }
        price
        abilities {
            strength
            agility
            intuition
            hp
            intelligence
            damage_min
            damage_max
        }
        
        image
        can_craft
        can_give
        can_repair
        serial_number
        individual_id
        
        armor {
            head
            chest
            abdomen
            girdle
            legs
        }
    }
`;

const paginatedStoreItemsList = gql`
    fragment PaginatedStoreItemsListFragment on PaginatedStoreItemType {
        data {
            _id
            item {
                ...ItemTypeFragment
            }
        }
        pagination {
            totalItemsCount
            totalPages
            page
            itemsPerPage
        }
    }

    ${item}
`;

export const paginatedUserItemsList = gql`
    fragment PaginatedUserItemsListFragment on PaginatedUserItemType {
        data {
            _id
            item {
                ...ItemTypeFragment
            }
            is_dressed_on
        }
        pagination {
            totalItemsCount
            totalPages
            page
            itemsPerPage
        }
    }

    ${item}
`;

export const user = gql`
    fragment UserFragment on User {
        id
        email
        login
        name
        sex
        birthday
        rank
        location {
            _id
            name
            label
            image
            can_attack
            can_create_fight
            is_default
            objects {
                name
                label
                image
                coordinates
            }
            nearby_locations {
                _id
                name
                label
                image
                can_attack
                can_create_fight
                is_default
            }
        }
        sets {
            name
            _id
            items {
                _id
                item {
                    ...ItemTypeFragment
                }
                is_dressed_on
            }
            is_dressed_on
        }
        abilities {
            backpack_weight_current
            damage_max
            damage_min
            spirituality
            sincerity
            intelligence
            endurance
            intuition
            agility
            strength
            basic_hp
            basic_damage_min
            basic_damage_max
            mall
            backpack_weight_max
            energy
            mall_type
            hp_current
            hp_max
        }
        armor {
            legs
            girdle
            abdomen
            chest
            head
        }
        mastery {
            sword
            knife
            axe
            club
        }
        modifiers {
            anti_critical
            critical
            anti_dodge
            dodge
            basic_critical
            basic_anti_critical
            basic_dodge
            basic_anti_dodge
        }
        login_attempts
        dinars
        valor
        draws
        defeats
        wins
        level
        exp
        free_mastery
        free_ability
        role
        hp_current
        hp_max
        items (limit: $itemsLimit, page: $itemsPage) @include(if: $withItems) {
            ...PaginatedUserItemsListFragment
        }
    }

    ${paginatedUserItemsList}
`;

export const store = gql`
    fragment StoreTypeFragment on StoreType {
        _id
        title
        slug
        tax
        items (limit: $itemsLimit, page: $itemsPage, filters:$itemsFilters) @include(if: $withItems) {
            ...PaginatedStoreItemsListFragment
        }
        myItems (limit: $itemsLimit, page: $itemsPage, filters:$itemsFilters) @include(if: $withMyItems) {
            ...PaginatedUserItemsListFragment
        }
    }

    ${paginatedStoreItemsList}

    ${paginatedUserItemsList}
`;
