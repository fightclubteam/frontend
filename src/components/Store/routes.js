import StoreBuy from './StoreBuy';
import StoreSell from './StoreSell';
import StoreItemCreate from './items/StoreItemCreate';

export default [
    {
        path: '/store/:slug',
        name: 'store',
        meta: { auth: true },
        component: StoreBuy,
        children: [],
    },
    {
        path: '/store/:slug/sell',
        name: 'store-sell',
        meta: { auth: true },
        component: StoreSell,
        children: [],
    },
    {
        path: '/store/item/create',
        name: 'store.item.create',
        meta: { auth: true },
        component: StoreItemCreate,
    },
];
