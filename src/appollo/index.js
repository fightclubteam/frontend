import { flatten, map, isEmpty } from 'lodash';
import ApolloClient, { createBatchingNetworkInterface } from 'apollo-client';
import { SubscriptionClient, addGraphQLSubscriptions } from 'subscriptions-transport-ws';
import store from '../store';
import processApolloErros from '../notifications';

const networkInterface = createBatchingNetworkInterface({
    uri: `//${process.env.API_HOST}/q`,
    batchInterval: 10,
});

networkInterface.use([{
    applyBatchMiddleware(req, next) {
        if (!req.options.headers) {
            req.options.headers = {};
        }
        const token = store.getters.token || localStorage.getItem('default_auth_token');
        if (!token) {
            return next();
        }

        req.options.headers.Authorization = `Bearer ${token}`;
        return next();
    },
}]);

networkInterface.useAfter([{
    applyBatchAfterware({ responses }, next) {
        const errors = flatten(map(responses, 'errors'));
        if (!isEmpty(errors)) {
            processApolloErros(errors);
        }
        next();
    },
}]);

const webSocketProtocol = location.protocol === 'https:' ? 'wss' : 'ws';
const wsClient = new SubscriptionClient(`${webSocketProtocol}://${process.env.API_HOST}/s`, {
    reconnect: true,
    lazy: true,
    connectionParams: {
        token: store.getters.token || localStorage.getItem('default_auth_token'),
    },
});

// wsClient.use([
//     {
//         applyMiddleware(operationOptions, next) {
//             const token = store.getters.token;
//             console.log(operationOptions, token);
//             if (token) {
//                 // eslint-disable-next-line
//                 operationOptions.Authorization = token;
//             }
//
//             next();
//         },
//     },
// ]);

const networkInterfaceWithSubscriptions = addGraphQLSubscriptions(
    networkInterface,
    wsClient,
);

const apolloClient = new ApolloClient({
    networkInterface: networkInterfaceWithSubscriptions,
    // @see https://www.graph.cool/blog/improving-performance-with-apollo-query-batching-ligh7fmn38/
    // shouldBatch: true,
    ssrForceFetchDelay: 100,
    connectToDevTools: true,
    // queryTransformer: addTypenameToDocument,
    // dataIdFromObject: r => r._id,
    queryDeduplication: true,
});

export default apolloClient;
