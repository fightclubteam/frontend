import Vue from 'vue';
import VueAuth from '@websanova/vue-auth';
import Popover from 'vue-js-popover';
import VueResource from 'vue-resource';
import VueApollo from 'vue-apollo';
import Vuelidate from 'vuelidate';
import { sync } from 'vuex-router-sync';
import VueNotifications from 'vue-notifications';
import miniToastr from 'mini-toastr';
import App from './App';
import router from './router';
import * as filters from './filters';
import store from './store';
import auth from './auth/auth';
import apolloClient from './appollo';

const apolloProvider = new VueApollo({
    defaultClient: apolloClient,
});

Vue.router = router;

miniToastr.init({
    timeout: 5000,
});

function toast({ title, message, type, timeout, cb }) {
    return miniToastr[type](message, title, timeout, cb);
}

Vue.use(VueResource);
Vue.use(Vuelidate);
Vue.use(VueApollo);
Vue.use(Popover, { tooltip: true });
Vue.use(VueAuth, auth);
Vue.use(VueNotifications, {
    success: toast,
    error: toast,
    info: toast,
    warn: toast,
});

Vue.http.options.root = `//${process.env.API_HOST}`;
Vue.config.productionTip = false;
Vue.config.devtools = true;

sync(store, router);

Object.keys(filters).forEach((key) => {
    Vue.filter(key, filters[key]);
});

const app = new Vue({
    el: '#app',
    store,
    router,
    apolloProvider,
    template: '<App/>',
    components: { App },
});

export { app, router, store };
