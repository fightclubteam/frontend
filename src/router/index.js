import Vue from 'vue';
import Router from 'vue-router';
import StoreRoutes from '@/components/Store/routes';

import * as gql from 'graphql-tag';
import apolloClient from '../appollo';

Vue.use(Router);

const router = new Router({
    mode: 'history',
    linkActiveClass: 'is-active',
    scrollBehavior: () => ({ y: 0 }),
    routes: [
        {
            path: '/',
            // eslint-disable-next-line
            component: resolve => require(['@/components/Root'], resolve),
            redirect: { name: 'inventory' },
            children: [
                {
                    path: '/inventory',
                    name: 'inventory',
                    meta: { auth: true },
                    // eslint-disable-next-line
                    component: resolve => require(['@/components/Inventory/Inventory'], resolve),
                },
                {
                    path: '/battle/:id',
                    name: 'battle',
                    meta: { auth: true },
                    // eslint-disable-next-line
                    component: resolve => require(['@/components/Fight/Battle'], resolve),
                },
                {
                    path: '/locations',
                    name: 'location',
                    meta: { auth: true },
                    // eslint-disable-next-line
                    component: resolve => require(['@/components/Location'], resolve),
                },
                {
                    path: '/battle/:id/log',
                    name: 'battle-full-log',
                    // eslint-disable-next-line
                    component: resolve => require(['@/components/Fight/BattleFullLog'], resolve),
                },
                {
                    path: '/fight',
                    name: 'fight',
                    // eslint-disable-next-line
                    component: resolve => require(['@/components/Fight/Fight'], resolve),
                    meta: { auth: true },
                    children: [
                        {
                            path: 'physical',
                            name: 'fight.physical',
                            // eslint-disable-next-line
                            component: resolve => require(['@/components/Fight/Physical'], resolve),
                            meta: { fight_type: 'PHYSICAL' },
                        },
                        {
                            path: 'group',
                            name: 'fight.group',
                            // eslint-disable-next-line
                            component: resolve => require(['@/components/Fight/Group'], resolve),
                            meta: { fight_type: 'GROUP' },
                        },
                        {
                            path: 'chaotic',
                            name: 'fight.chaotic',
                            // eslint-disable-next-line
                            component: resolve => require(['@/components/Fight/Chaotic'], resolve),
                            meta: { fight_type: 'CHAOTIC' },
                        },
                        {
                            path: 'current',
                            name: 'fight.current',
                            // eslint-disable-next-line
                            component: resolve => require(['@/components/Fight/Current'], resolve),
                            meta: { fight_type: 'CURRENT' },
                        },
                        {
                            path: 'finished',
                            name: 'fight.finished',
                            // eslint-disable-next-line
                            component: resolve => require(['@/components/Fight/Finished'], resolve),
                            meta: { fight_type: 'FINISHED' },
                        },
                    ],
                },
                ...StoreRoutes,
            ],
        },
        {
            path: '/login',
            name: 'login',
            meta: { auth: false },
            // eslint-disable-next-line
            component: resolve => require(['@/components/Auth/Login'], resolve),
        },
        {
            path: '/signup',
            name: 'signup',
            meta: { auth: false },
            // eslint-disable-next-line
            component: resolve => require(['@/components/Auth/Signup'], resolve),
        },
        {
            path: '/info/:login',
            name: 'info',
            // eslint-disable-next-line
            component: resolve => require(['@/components/CharacterInfo'], resolve),
        },
        {
            path: '*',
            // eslint-disable-next-line
            component: resolve => require(['@/components/NotFoundComponent'], resolve),
        },
    ],
});

router.beforeEach((to, from, next) => {
    if (to.name === 'battle' || !to.meta || !to.meta.auth || !Vue.prototype.$auth.check()) {
        next();
    } else {
        apolloClient.query({
            query: gql`{getCurrentBattleId}`,
            fetchPolicy: 'network-only',
        })
        .then(({ data: { getCurrentBattleId } }) => {
            if (getCurrentBattleId) {
                if (from.name === 'battle') {
                    next(false);
                } else {
                    next({
                        name: 'battle',
                        params: { id: getCurrentBattleId },
                    });
                }
            } else {
                next();
            }
        })
        .catch(next);
    }
});

export default router;
