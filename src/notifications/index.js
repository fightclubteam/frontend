import VueNotifications from 'vue-notifications';
import * as _ from 'lodash';

export default (errors) => {
    _.each(errors, (error) => {
        switch (_.get(error, 'code')) {
            case 'FightNotFoundError':
                VueNotifications.error({ message: 'Запрашиваемый бой не найден.' });
                break;
            case 'FightStatusError':
                VueNotifications.error({ message: 'Запрашиваемый бой уже завершился.' });
                break;
            default:
                break;
        }
    });
};
