/* eslint no-param-reassign: ["error", { "props": false }] */

import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
    strict: process.env.NODE_ENV !== 'production',
    // actions,
    // getters,
    modules: {
        // app,
        // menu
    },
    state: {
        token: null,
        user: {},
    },
    getters: {
        token(state) {
            return state.token;
        },
        user(state) {
            return state.user;
        },
    },

    mutations: {
        SET_TOKEN(state, token) {
            state.token = token;
        },
        CLEAR_TOKEN(state) {
            state.token = null;
        },
        SET_USER(state, user) {
            state.user = user;
        },
        CLEAR_USER(state) {
            state.user = {};
        },
    },
});

export default store;
