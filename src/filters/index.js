import moment from 'moment';

export function date(value, format) {
    return moment(new Date(value)).format(format);
}

export function trim(value) {
    return value.trim();
}

export function zeroPad(i) {
    return i < 10 ? `0${i}` : i;
}

// export function fromHtmlEntities(string) {
//     return (string || '').replace(/&#\d+;/gm, str => String.fromCharCode(str.match(/\d+/gm)[0]));
// }

export function numberThousand(value) {
    /* eslint-disable no-param-reassign */
    value = (value || 0).toString();
    const d = value.indexOf('.');
    let s2 = d === -1 ? value : value.slice(0, d);

    for (let i = s2.length - 3; i > 0; i -= 3) {
        s2 = `${s2.slice(0, i)} ${s2.slice(i)}`;
    }

    if (d !== -1) {
        s2 += value.slice(d);
    }

    return s2;
}

export function battleType(value) {
    return {
        FISTFUL: 'ФИЗИЧЕСКИЙ',
        WEAPON: 'С ОРУЖИЕМ',
    }[value];
}

export function capitalize(value) {
    if (typeof value !== 'string' || !value.length) {
        return '';
    }
    return value.charAt(0).toUpperCase() + value.slice(1).toLowerCase();
}

export function abilityPad(value) {
    return value < 0 ? value : `+${value}`;
}
