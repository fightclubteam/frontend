module.exports = {
    gzip: true,
    sourceMap: false,
    env: {
        NODE_ENV: '"production"',
        API_HOST: '"backend.sacralium.ru"',
    },
};
