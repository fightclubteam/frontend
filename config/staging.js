module.exports = {
    gzip: true,
    sourceMap: true,
    env: {
        NODE_ENV: '"staging"',
        API_HOST: '"backend.bkgame.click"',
    },
};
