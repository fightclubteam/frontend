module.exports = {
    host: 'localhost',
    port: 8080,
    autoOpenBrowser: true,
    proxyTable: {},
    sourceMap: true,
    env: {
        NODE_ENV: '"development"',
        API_HOST: '"localhost:8081"',
    },
};
